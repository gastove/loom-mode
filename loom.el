;;; loom.el --- A major mode for the Loom programming language
;;
;;; Commentary:
;;
;;; Code:

(require 'clojure)

(define-derived-mode loom-mode clojure-mode
  "Loom"
  "A major mode for editing Loom source files.")

(provide 'loom)
;;; loom.el ends here

;;; -*- mode: clojure; -*-

;;-------------------------Types and Type Annotations---------------------------
;;
;; What about a "type pair"?
;; -- can we infer these, and in what contexts?
;; -- if we use <> as a pair, seems like we need gt/lt as funcs, which is... sub-optimal
;; -- -- Update: do not do this.
;;
;; When I do this, it looks like so:
{int a}  ;; <- Yes, or at least... maybe?
%{int a} ;; <- No
${int a} ;; <- Very no
(a . b)  ;; pair
(a -> b) ;; typed pair? <- Closer

;; What about a language of type symbols?
;; -> | Can be either a type declaration or a cast
;; => | Functions and transducers, maybe?

(def foo 5)                                    ;; Symbol foo inferred to have type int
(def int->foo 5)                               ;; Symbol foo is declared to have type int
(def bar foo->int)                             ;; The value of symbol foo is cast to int, then returned
(defn foo [int => int bar, str->baz] => int)   ;; Takes a function from int to int bar and string baz, returns int
(defn foo [(fn [int] => int) bar, str->bar] => int) ;; Same idea as previous
(defn foo [fn->foo]) ;; Take an untyped function called foo, locally
(defn foo [] => {:flang -> int :spang -> str}) ;; Returns a map where keywords map to specified values
(defn foo [] => {:of [int str]})               ;; Maaaybe... typing the *keys*?

;;----------------------------------Contracts-----------------------------------
;; Okay wait hangon what about the notion of "contracts." That is: int->foo is a
;; fairly satisfying way of expressing that the symbol `foo` will have type
;; `int`. But what about more complex kinds of guarantees, like function
;; signatures?
(defc foo [int->foo] => int)          ;; Define a contract
(defn baz [foo=>bar])                 ;; `bar` must fulfill contract `foo`.
(defn bar #=>foo [int->beep] => int)  ;; `bar` must fulfill contract `foo`
;; The above should be able to be elided to this shortened form, omitting information from the contract as desired
(defn bar #=>foo [beep])              ;; omit explicit types
(defn bar #=>foo)                     ;; Full inference from the contract

;; This is all pretty woobly yet, but I think it's moving me closer to where I want to be.


;; There are a lot of weird intricacies here; the one I am presently the most
;; keenly aware of is the way in which the => operator breaks normal LISP
;; evaluation. (That is, it's an infix, rather than a prefix, operator.) It is,
;; on the up-side, very easy to read (for me), which I of course like very much.

;;-------------------------------Data Structures---------------------------------

;; Uh okay so how do we typed data structures?
;; Defining a data structure literal does not involve specifying types.
(def flarb {:poot 5}) ;; Gotta infer the type of key :poot

;; Typed records
(defrecord Foo [int->bar str->baz]) ;; Type the constructor of a record

;;---------------------------------Sample Code-----------------------------------
;;
;; Here's a common one: read JSON out of a file, parsing each line and returning
;; a vector of parsed results
(def json-path "/path/to/the/file") ;; Type inference marks `json-path` as a string
;; So how about opening a file?
(def json-file (open json-path)) ;; Type inferred to File; mostly very Clojure
(def json-file (File json-path)) ;; Hrmmm


(defn process-file [File->file]
  (with-open [handle file]
    (map #(-> %
              (parse-json)
              (select-keys [:foo :bar :baz]))
         (line-seq handle))))

(defc processor-contract [string->json] => list->map[keys]) ;; oh god

(def foo [int->a float->b rational->c/d] -> int
  "I am a docstring, yes I am."
  (-> a
      (cycle 12)
      (reduce #(+ % %2))
      (+ b c)))
